# dev-tools
Orbos development tools.

## Installation & Update instructions

### Docker

#### Install & Update are the same.

docker pull orbos/archlinux:orbos

#### Docker Usage

docker run -it orbos/archlinux:orbos

### Manual installation.

mkdir ~/orbos

cd ~/orbos

git clone https://github.com/orbos/dev-tools.git

cd dev-tools

git checkout experimental

sudo ./install or su -c ./install


#### Update instructions

If you see that I've pushed code to github you can go ahead and update your
script base on the host system to get the newest framework.

cd ~/orbos/dev-tools

git pull

sudo ./install or su -c ./install
