#!/usr/bin/env bats

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd nodelay
}
@test "nodelaymsg - msg accuracy test." {
 run msg "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd nodelay
}
@test "nodelaymsg - man page output msg accuracy test." {
 ManArray=$( man wc )
 run msg "${ManArray[@]}"
 [ "$output" = "${ManArray[@]}" ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd nodelay
}
@test "nodelaymsg - qmsg accuracy test." {
 run qmsg "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd nodelay
}
@test "nodelaymsg - rmsg accuracy test." {
 run rmsg "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd nodelay
}
@test "nodelaymsg - msgr accuracy test." {
 run msgr "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd nodelay
}
@test "nodelaymsg - rmsgr accuracy test." {
 run rmsgr "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd nodelay
}
@test "nodelaymsg - qmsgr accuracy test." {
 run qmsgr "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd nodelay
}
@test "nodelaymsg - rqmsg accuracy test." {
 run rqmsg "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd nodelay
}
@test "nodelaymsg - rqmsgr accuracy test." {
 run rqmsgr "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}