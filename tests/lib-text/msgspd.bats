#!/usr/bin/env bats

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
}
@test "fastmsg enable test." {
  run msgspd fast
  run head -n 17 "$HOME/gitlab/dev-tools/conf/lib-text.conf" | tail -n 1
  [ "$output" != *"#fastmsg"* ]
}
teardown() {
  cleanup_libtext
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
}
@test "slowmsg enable test." {
  run msgspd slow
  run head -n 18 "$HOME/gitlab/dev-tools/conf/lib-text.conf" | tail -n 3 | head -n 1
  [ "$output" != *"#slowmsg"* ]
}
teardown() {
  cleanup_libtext
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
}
@test "nodelaymsg enable test." {
  run msgspd no
  run head -n 18 "$HOME/gitlab/dev-tools/conf/lib-text.conf" | tail -n 1
  [ "$output" != *"#nodelaymsg"* ]
}
teardown() {
  cleanup_libtext
}