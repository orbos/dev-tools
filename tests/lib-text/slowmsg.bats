#!/usr/bin/env bats

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd slow
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "slowmsg - msg accuracy test." {
 run msg "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd slow
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
  ManArray=$( man wc )
}
@test "slowmsg - man page output msg accuracy test." {
 run msg "${ManArray[@]}"
 [ "$output" = "${ManArray[@]}" ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd slow
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "slowmsg - qmsg accuracy test." {
 run qmsg "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd slow
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "slowmsg - rmsg accuracy test." {
 run rmsg "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd slow
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "slowmsg - msgr accuracy test." {
 run msgr "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd slow
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "slowmsg - rmsgr accuracy test." {
 run rmsgr "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd slow
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "slowmsg - qmsgr accuracy test." {
 run qmsgr "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd slow
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "slowmsg - rqmsg accuracy test." {
 run rqmsg "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}

setup() {
  . "$HOME"/gitlab/dev-tools/lib/lib-text
  msgspd slow
  commaspeed="0"
  periodspeed="0"
  speed="0"
  linespeed="0"
}
@test "slowmsg - rqmsgr accuracy test." {
 run rqmsgr "normal text, test."
 [ "$output" = "normal text, test." ]
}
teardown() {
  cleanup_libtext
  cleanup_libtextconf
}