#!/usr/bin/env bash

if [[ $(type -t cleanup_libbuild) ]]
  ## Unsets lib-build before defining a new version if it is reloaded.
  then
  cleanup_libbuild
fi
# shellcheck source=/dev/null
. csl
csl "${BASH_SOURCE[0]}"
. "$conf"/orbos.conf

reload_libbuild()
{
  # shellcheck source=/dev/null
  . "$lib"/lib-build
}
libbuildfunctions+=("reload_libbuild")

clean_source() ## Cleans the package source for the loaded orb file name.
{
  for dir in ${pkg[name]}*
  ## This for loop checks the names of sources sent to it.
  ## This is wildcarded in order to clean the sources without needing the
  ## version number.
  do
    if [[ -d "$dir" ]] ## Make sure its a directory.
      then
      rm -rfv "$dir" ## Remove the directory.
    fi
  done
}
libbuildfunctions+=("clean_source")

clean_source_dirs() ## removes all directories in a directory.
{
  # shellcheck disable=SC2154
  for dir in "$src"/*
  do
    if [[ -d "$dir" ]] ## Make sure its a directory.
      then
      rm -rfv "$dir" ## Remove the directory.
    fi
  done
}
libbuildfunctions+=("clean_source_dirs")

loadmod()
## Accepts one argument.
## This should be the build suffix name to load from that mod file.
{
  # shellcheck disable=SC2154
  modfile="$orbs/${pkg[name]}/orb.mod"
  if [[ -f "$modfile" ]]
    then
    # shellcheck source=/dev/null
    . "$modfile"
    if [[ $(type -t "$1") ]]
      ## Check if the build suffix sent actually has extra commands.
      then
      "$1"
    fi
  fi
}
libbuildfunctions+=("loadmod")
libbuildvariables+=("modfile")

checklink()
## Checks to make sure the download link is valid.
## This function is probably going to be depreciated using cURL/aria2c + xml2
{
  if wget --spider -q "${pkg[dlurl]}"
  then
    ret=$?
    printf '%s %s\n' "${pkg[dlurl]}" "exists!"
  else
    ret=$?
    #printf '%s %s\n' "${pkg[dlurl]}" "does not exist!"
  fi
  return "$ret"
}
libbuildfunctions+=("checklink")

expandlink()
## Disassembles the download url for a package into information that is easier
## to work with. It also allows us to grab less information from the user.
{
  ## Removes inclusively everything before the first / from the dlurl.
  ## This gives us the entire filename.
  file=${dlurl##*/}

  ## Removes the first - and everything after it to get the name.
  name=${file%-*}

  ## Removes everything before and after the version number.
  version=${file#*-}
  version=${version%$tar*}

  ## The extension is everything after the version at this point.
  extension=${file#*$version}

  ## Write the base url to where all package versions are stored.
  ## file name version extension url for getting version updates.
  url=${dlurl%/*}/ 
}
libbuildfunctions+=("expandlink")
libbuildvariables+=("file" "name" "version" "extension" "url")

dispelinkinfo()
{
  printf '%s : %s\n%s : %s\n%s : %s\n%s : %s\n%s : %s\n' \
         "name" "$name" \
         "version" "$version" \
         "extension" "$extension" \
         "file" "$file" \
         "url" "$url"
}
libbuildfunctions+=("dispelinkinfo")

combinelink()
## Combine disassembled information into a full download url.
{
  ## Combine the version name.
  file="$name-$version$extension"
  ## Create a new dlurl from the potentially modified filename.
  dlurl="$url$file"
}
libbuildfunctions+=("combinelink")

pd()
## Keeps track of directories that have been pushed to the stack during
## the compilation process so we can pop all of them off the stack.
{
  pushd "$1"
  ((pdcount++))
}
libbuildfunctions+=("pd")
libbuildvariables+=("pdcount")

pds()
## Pops all the directories off the stack.
{
  for (( ; pdcount > 0 ; pdcount-- ))
  do
    popd
  done
}
libbuildfunctions+=("pds")

# expandver()
# {
#   if [ "$version" = "" ]
#   then
#     version=${pkg[version]}
#   fi
#
#   major=${version%%.*}
#
#   minor=${version#$major.}
#   minor=${minor%.*}
#
#   build=${version#$major.$minor.}
#
#   if [ $build = $version ]
#   then
#     build=""
#   fi
# }

# combinever()
# {
#   if [ "$build" != "" ]
#   then
#     version="$major.$minor.$build$@"
#     if [ "$release" != "" ]
#       then
#       version="$major.$minor.$build.$release"
#     else
#       version="$major.$minor"
#     fi
#   fi
#   pkg[version]="$version"
# }

## This might be a viable way to do it but there might be an easier way.
# getsource()
# # This isnt actually used now so I am commenting it.
# {
#     msg '%s %s %s' "Downloading sources for" "${pkg[name]}" "..."
#     dl=("${pkg[dlurl]}")
#     read -a bdeps <<< "${pkg[bdeps]}"
#     for pkgname in "${bdeps[@]}"
#     do
#       lpi "$pkgname"
#       dl+=("${pkg[dlurl]}")
#     done
#     lpi "$1"
#     wget --continue "${pkg[dlurl]}"
# }
# libbuildvariables+=("dl" "bdeps")
# libbuildfunctions+=("getsource")

getartifacts()
## Takes 1 or 2 arguments.
## The first argument should be a link.
## The second should be a pkg group.
## Downloads all artifacts in an artifacts folder.
## To get an appropriate link browse artifacts from a gitlab build then enter
## the directory based on the build tag, Copy the URL from there.
{
  pau() ## Parse Artifacts URL.
  {
    url="$1" ## Defines url as the first argument passed and only accepted.
    url=${url%/*/*/*/*}
    artifactsurl="$1"
    
  
    ## We start working from the end of the URL.
    #tag=${url%/} ## Removes the trailing /
    #tag=${tag##*/} ## Removes the text before the /
    #build=${url%/*/*/*/} ## removes the first three entries after the /
    #build=${build##*/}
    #repo=${url%/*/*/*/*/*/}
    #repo=${repo##*/}
    #group=${url%/*/*/*/*/*/*/}
    #group=${group##*/}
    #url=${url%/*/*/*/*/*/*/*/}
    
    ## Since url is no longer what we started with we create a new version of it.
    #artifactsurl="$url/$group/$repo/builds/$build/artifacts/browse/$tag/"
  }

  get_pkgnames()
  ## get the names of the packages we're going to download if limited.
  {
    unset -v includepkg
    if [ -f "$groups/$2.dl" ]
    ## If the person has sent a limiting factor check if it exists and limit.
    then
      while IFS= read -r pkgname; do ## Scan the limiting file.
        includepkg+=("$pkgname")
      done < "$groups/$2.dl"  
    fi
  }
  
  parse_url()
  ## parse the HTML into usable URLS.
  {
    if [[ "$href" != "<a" ]]
    then
      dla=${href#*\"*\"*\"} ## Remove the first quote and all prior text. 
      dla=${dla%%\"*} ## Remove the second quote and all subsequent text.
      dlap=${dla%/*/*/*}
      dlas=${dla#/*/*/*/}
      dla="$dlap"'/raw/'"$dlas"
      dls+=("$url$dla") ## Combine the download artifact with the URL for a source.
    fi
  }

  artifact_index()
  ## Grab the index html file and parse out the lines we need.
  {
    
    ## Ensure nothing is contained in the variables we are going to store our data in.
    unset -v dla dls filenames
    ## Each entry stored in the arrays are going to be split by new lines now.
    oIFS=$IFS
    IFS=$'\n'
    artifact_index=( $(curl -s "$artifactsurl" | grep title | grep blob | grep href) )
    for href in "${artifact_index[@]}"
    do
      parse_url ## Parse each line into something usable.
    done
    IFS=$oIFS ## Reset looping to use spaces.
  }

  group_downloads()
  # Groups the downloads into an array of downloadable items.
  {
    unset -v groupdl
    if [ -f "$groups/$2.dl" ]
    ## If limiting to only a group it will add those.
    then
      get_pkgnames "$@"
      for pkgname in "${includepkg[@]}"
      do
        groupdl+=($(printf '%s\n' "${dls[@]}" | grep -E "${pkgname}"))
      done
    else ## Otherwise add everything.
      groupdl=("${dls[@]}")
    fi
  }

  get_filename()
  ## Split the filename off the dlurl.
  {
    filename=${dlurl##*/}
    export filename
  }

  download_group()
  # Downloads the grouped array entries.
  {
    unset first
    for dlurl in "${groupdl[@]}"
    ## Scale through the downloads one at a time.
    do
      ## Create a count of the number of lock files currently in existence.
      count=$(find "$src" -name "*.lock" | wc -l)
      if (( "$count" >= "$maxdlcount" )) ## maxdlcount comes from orbos.conf
      ## Uses the number of lock files that exist to limit the # of concurrent
      ## downloads able to happen at one time.
      then
        while (( "$count" >= "$maxdlcount"))
        ## Lock the process in a loop to prevent spawning more download processes.
        do
          sleep 1s
          count=$(find "$src" -name "*.lock" | wc -l)
        done
      fi
      
      get_filename
      aria2c -D -d "$src" --on-download-start="/usr/bin/orbos/lock-dl" --on-download-complete="/usr/bin/orbos/lock-dl" -c "$dlurl"
      sleep 1s
      
      if [[ "$first" != "done" ]]
      ## This section relates to pausing the spawning of more downloads when
      ## downloading the first tarball. Subsequent downloads will happen during
      ## The first compilation.
        then
          if [[ "$src/$filename" = *".tar."* ]]
          then
            while [ -f "$src/.$filename".lock ]
            do
              sleep 1s
            done
          first="done"
          fi
        fi  
    done
  }
  
  getartifacts_run()
  ## Run the processes in this function.
  {
    pau "$@"
    artifact_index
    group_downloads "$@"
    download_group 
  }
  
  getartifacts_run "$@"
}
libbuildvariables+=("url" "tag" "build" "repo" "group" "artifactsurl" "dla" "dls" "artifact_index" "count" "href" "pkgname" "includepkg" "groupdl" "filename" "first" "dlap" "dlas")
libbuildfunctions+=("getartifacts" "pau" "get_pkgnames" "parseurl" "artifact_index" "group_downloads" "get_filename" "download_group" "getartifacts_run")

# getsource()
# # This works for now but I think the previous version will work better once we
# # have all the appropriate packages. Right now this wastes time having wget
# # check if a binary exists or not. Doing it in shell would be faster.
# {
#     msg "Downloading sources for ${pkg[name]} ..."
#     read -a bdeps <<< "${pkg[dlurl]}"
#     wget --continue "${bdeps[@]}"
# }
# libbuildvariables+=("bdeps")
# libbuildfunctions+=("getsource")

setup_build_dir()
## Creates a build directory if one is needed and enters it.
{
  mkdir -pv "$src/${pkg[name]}-build" ## Create the directory.
  pd "$src/${pkg[name]}-build" ## Enter it.
}
libbuildfunctions+=("setup_build_dir")

extract()
## Extract the source code for compilation and enter the directory.
{
    tar -xf "${pkg[file]}" ## Extract the sources.
    pd "$src/${pkg[name]}"*/ ## Enter the source directory.
}
libbuildfunctions+=("extract")

extractn()
## Extracts via the name given to it then enters the directory.
{
  tar -xf "$src/$1"*.tar.*
  pd "$src/$1"-*
}
libbuildfunctions+=("extractn")

extract_extras()
## Extracts any extras of the name sent to it. Later this will refer to the 
## orb specific to what is being extracted and the apporpriate version will
## be chosen.
{
  for names in "$@"
  do
    tar -xf "$src/$names"-*.tar.*
    for dir in "$names"-*
    do
      if [[ -d "$dir" ]]
        then
        local newdir=${dir%-*}
        mv -v "$dir" "$newdir"
      fi
    done
  done
}
libbuildfunctions+=("extract_extras")
libbuildvariables+=("names" "dir" "newdir")

stripbin()
## Takes one argument of a directory prefix modifier.
## Strip debugging and unneeded symbols from binaries.
{
  strip --strip-debug "$1"/lib/*
  /usr/bin/strip --strip-unneeded "$1"/{,s}bin/*
}
libbuildfunctions+=("stripbin")

stripdoc()
## Takes one argument of a directory prefix modifier.
## Strip the man pages.
{
  rm -rf "$1"/{,share}/{info,man,doc}
}
libbuildfunctions+=("stripdoc")

stripall()
## Strips both binaries and documentation
{
  stripbin "$1"
  stripdoc "$1"
}
libbuildfunctions+=("stripall")

set_pkginfo()
## Sets the pkg associative array info.
{
  declare -Ag pkg=(
    [name]="$name"
    [version]="$version"
    [description]="$description"
    [homepage]="$homepage"
    [extension]="$extension"
    [dlurl]="$dlurl"
    [url]="$url"
    [file]="$file"
    [bdeps]="$bdeps"
    [cdeps]="$cdeps"
    [checksum]="$checksum"
  )
}
libbuildfunctions+=("set_pkginfo")

backup_files()
## Takes multiple arguments. Each argument can be an orb or orb.spec file.
## Will incrementally backup orb and orb.spec files.
{
  for filename in "$@"
  do
    if [[ -f "$filename" ]]
      ## If the filename file already exists then back it up.
      then
      if [[ -f "$filename.backup" ]]
        ## If the backup file already exists then find the increment.
        then
        for (( i=1 ; ; i++ ))
        do
          if [[ ! -f "$filename.backup$i" ]]
            ## Find the one that does not exist and move the file there
            ## and break out of the infinite loop.
            then
            lastbackup="$filename.backup$i"
            mv "$filename" "$filename.backup$i"
            break
          fi
        done
      else
        ## if no backup exists use the basic name.
        lastbackup="$filename.backup"
        mv "$filename" "$filename.backup"
      fi
    fi
  done
}
libbuildfunctions+=("backup_files")
libbuildvariables+=("filename" "i" "lastbackup")

ppi()
## Print pkg info.
{
  # shellcheck disable=SC2154
    for key in "${pkgkeys[@]}"
    ## Load each key from the pkgkeys array. This is defined in orbos.conf
    do
      local pkgmsg=("$key" ":" "${pkg[$key]}")
      ## Print the key and the entry in a nicely formatted way.
      qmsgr "${pkgmsg[@]}"
    done
    msg " "
}
libbuildfunctions+=("ppi")
libbuildvariables+=("key" "pkgmsg")

ask_pkginfo()
## Interactively gathers any info required to write the pkg associateive array.
## This gathers general information about the package, a description,
## The homepage and download url.
## In the future some dependency information may be kept here.
## Adding the orbos text library functionality did add lines of code but it
## seems worth it in terms of the polished feel of the final result.
{
  unset -v dlurl homepage description bdeps cdeps checksum
  qmsg "What is the url to download the package?"
  ## We ask for the download url which is the most important information
  ## to be as accurate as possible.
  read -r dlurl
  ## expandlink expands dlurl into: $file $name $version $extension $url
  ## This allows us to ask for a little information and provide a orb file
  ## containing far more than the user had to enter.
  expandlink
  dispelinkinfo
  qmsg "Enter the name of any that are incorrect:"
  read -r -a bad
  declare -a badfixes
  for badentry in "${bad[@]}"
  do
    ref="$badentry"
    qmsg "Please enter the correction for $badentry:"
    read -r newentry
    badfixes+=("$newentry")
    eval "$ref=$newentry"
  done
  qmsg "What is the project homepage for the package?"
  ## What is the appropriate homepage to give credit to the project
  ## I suppose later we could use this entry for seeing how many packages
  ## come from a specific project.
  read -r homepage
  qmsg "Give a description of the package(Check the project homepage)?"
  ## Knowing what you are installing does is good!
  read -r description
  qmsg "What are the build dependencies for the package?"
  ## What do we need to actually build the files? This will be used later.
  read -r bdeps
  qmsg  "What are the check dependencies for the package?"
  ## Some dependencies are only required for the check process.
  read -r cdeps
  qmsg "What are the dependencies for the package?"
  ## What dependencies are required for this to be installed.
  read -r deps
  qmsg "What are the optional dependencies for the package?"
  ## What dependencies are optional to add features.
  read -r odeps
  qmsg "Checksum of source archive(Can be left blank)?"
  ## Knowing a checksum is good.
  read -r checksum
  
  ## Update the pkg associative array with the information we just obtained.
  set_pkginfo
  # rqmsgr "Here is the data stored in the pkg array right now:"
  # msgr
  # ppi
  
  ## Create directory, set file locations & touch the orb file.
  mkdir -p "$orbs/${pkg[name]}" ## create the directory needed for the orb file.
  orb="$orbs/${pkg[name]}/orb" ## Set the location of the orb file.
  spec="$orb.spec" ## Set the location of the orb.spec file.
  
  backup_files "$spec" "$orb"
  
  ## Write the spec file for this section.
  ## Any > or >> "$spec" entry is writing user input to the spec file.
  ## In the case of an array we send "${array[*]}" to write the array onto
  ## a single line.
  { ## Using these brackets allows me to write all outputs in order more clearly
    printf '%s\n' "$dlurl" 
    printf '%s\n' "${bad[*]}"
    if [[ "${bad[*]}" != "" ]]
    then
      printf '%s\n' "${badfixes[@]}"
    fi
    printf '%s\n' "$homepage"
    printf '%s\n' "$description"
    printf '%s\n' "$bdeps"
    printf '%s\n' "$cdeps"
    printf '%s\n' "$deps"
    printf '%s\n' "$odeps"
    printf '%s\n' "$checksum"
  } > "$spec"
}
libbuildfunctions+=("ask_pkginfo")
libbuildvariables+=("dlurl" "homepage" "description" "bdeps" "cdeps" "checksum"
                    "pkg" "orb" "spec" "deps" "odeps" "bad" "badentry" 
                    "newentry" "badfixes" "ref")

ask_build()
## Asks all build info used in writing the build functions containing
## buildcmd which keeps track of the order things are run.
## buildopts which stores how everything is built.
{
  qmsg "If this build needs a name suffix please enter it:"
  ## This sets the suffix in the function created in the orb file for this build.
  read -r buildsuffix
  printf '%s\n' "$buildsuffix" >> "$spec"
  ## Add description code here.
  qmsg "Please enter a description for this build:"
  read -r bdesc
  printf '%s\n' "$bdesc" >> "$spec"
  rqmsgr "Here is the data in the pkg array you may need to modify:"
  ## Add pkg modifiers here.
  ppi
  qmsg "Which of these do you need to modify(version bdeps cdeps)?"
  read -r -a pkgmod
  printf '%s\n' "${pkgmod[*]}" >> "$spec"
  for key in "${pkgmod[@]}"
  do
    commandmsg=(
      "What would you like the"
      "$key"
      "field in the pkg array to be?"
    )
    qmsg "${commandmsg[@]}"
    read -r newpkginfo
    printf '%s\n' "$newpkginfo" >> "$spec"
    pkg["$key"]="$newpkginfo"
  done
  loadmod "$buildsuffix"
  rmsgr "Here are all the shorthand options and what they do:"
  # shellcheck disable=SC2154
  for command in "${prebuildcmds[@]}" "${buildcmds[@]}" "${extrafxn[@]}"
  ## When I tried to pass a line of:
  ## "$command : ${buildopts[$command-pre]} ${buildopts[$command]}} ${buildopts[$command-suf]}"
  ## Niche display options like this require an array to be passed to qmsg
  ## because sending it to msg will produce each entry on a new line.
  ## We cannot use something like qmsg "    ${cmddesc[$command]}" because
  ## the fastmsg case will toss out the whitespace as not being a word.
  ## In cases where indentation is needed to increase readability I use --->
  ## If you need a manual newline in your display run msg " "
  do
    commandmsg=(
      "$command"
      ":"
      "${buildopts[$command-pre]}"
      "${buildopts[$command]}"
      "${buildopts[$command-suf]}"
    )
    qmsg "${commandmsg[@]}"
    rqmsgr "---> ${cmddesc[$command]}"
  done
  rqmsg "What options do you need to compile this package(Space Delimited)?"
  ## This tells the system all commands to be run.
  read -r -a buildcmd
  printf '%s\n' "${buildcmd[*]}" >> "$spec"
  qmsg "What options do you need to modify(Space Delimited)?"
  ## This tells the system which commands you want to modify.
  read -r -a editcmd
  printf '%s\n' "${editcmd[*]}" >> "$spec"
  for command in "${editcmd[@]}"
  do
    qmsg "Input custom $command command if needed(Default=${buildopts[$command]})?"
    ## Reads a custom command to be run.
    read -r newcommand
    printf '%s\n' "$newcommand" >> "$spec"
    qmsg "Prefix appendage for ${buildopts[$command]}(Default=${buildopts[$command-pre]})?"
    ## Reads custom prefix command information.
    read -r prefix
    printf '%s\n' "$prefix" >> "$spec"
    qmsg "Suffix appendage for ${buildopts[$command]}(Default=${buildopts[$command-suf]})?"
    ## Reads custom suffix command information. Often this will be command options.
    read -r suffix
    printf '%s\n' "$suffix" >> "$spec"
    if [[ "$newcommand" != "" ]]
      ## We dont set the entered data unless something was actually entered.
      then
      ## Sets the custom command to be run.
      buildopts["$command"]="$newcommand"
    fi
    if [[ "$prefix" != "" ]]
      ## We dont set the entered data unless something was actually entered.
      then
      ## Sets the prefix of the command to be run.
      buildopts["$command"-pre]="${buildopts[$command-pre]} $prefix"
    fi
    if [[ "$suffix" != "" ]]
      ## We dont set the entered data unless something was actually entered.
      then
      ## Sets the suffix of the command to be run.
      buildopts["$command"-suf]="${buildopts[$command-suf]} $suffix"
    fi
  done
}
libbuildfunctions+=("ask_build")
libbuildvariables+=("buildsuffix" "buildcmd" "editcmd" "newcommand" "prefix"
                    "suffix" "buildopts" "commandmsg" "pkgmod" "newpkginfo"
                    "bdesc")

cleanup_orbfiles()
## clean up variables and functions set by orb files.
{
  # shellcheck disable=SC2154
  for function in "${info[@]}" "${builds[@]}"
  ## info and builds arrays hold every function in the orb file.
  do
    unset -f "$function" ## Unset the function
  done
  ## Unset the variables last because the builds and info arrays hold
  ## the function information.
  unset -v pkg info builds buildcmd buildopts bdesc _
}
libbuildfunctions+=("cleanup_orbfiles" "cleanup_libbuild")
libbuildvariables+=("function" "libbuildfunctions" "libbuildvariables")

readorbspec()
## Accepts a orb.spec file location.
## Read a orb.spec file containing all questions asked into a array.
## expand that array into everything needed to write orb file.
{
  unset -v lines orbspec
  while read -r line
  do
    orbspec+=("$line") ## Set each line of an orb.spec file into an array.
  done < "$1" ## Loads the spec file into the while loop.
  count=0
}
libbuildfunctions+=("readorbspec")
libbuildvariables+=("orbspec")

orbspecpkginfo()
## Read the pkg info from the file.
{
  dlurl="${orbspec[$count]}" ## Set the download url.
  ((count++))
  expandlink
  read -r -a bad <<< "${orbspec[$count]}"
  ((count++))
  for badentry in "${bad[@]}"
  do
    ref="$badentry"
    eval "$ref=\"${orbspec[$count]}\""
    ((count++))
  done
  homepage="${orbspec[$count]}" ## set the homepage.
  ((count++))
  description="${orbspec[$count]}" ## Description of what the package does.
  ((count++))
  bdeps="${orbspec[$count]}" ## build deps.
  ((count++))
  cdeps="${orbspec[$count]}" ## check deps.
  ((count++))
  deps="${orbspec[$count]}" ## deps.
  ((count++))
  odeps="${orbspec[$count]}" ## optional deps.
  ((count++))
  checksum="${orbspec[$count]}" ## Checksum.
  ((count++))
  set_pkginfo
  
  orb="$orbs/${pkg[name]}/orb" ## Set the location of the orb file.
  spec="$orb.spec"
  
  backup_files "$orb"
}
libbuildfunctions+=("orbspecpkginfo")
libbuildvariables+=("count")

orbspecbuild()
{  
  buildsuffix="${orbspec[$count]}"
  ((count++))
  loadmod "$buildsuffix"
  bdesc="${orbspec[$count]}"
  ((count++))
  OIFS="$IFS"
  IFS=' '
  read -r -a pkgmod <<< "${orbspec[$count]}"
  ((count++))
  
  for key in "${pkgmod[@]}"
  do
    pkg["$key"]="${orbspec[$count]}"
    ((count++))
  done
  
  read -r -a buildcmd <<< "${orbspec[$count]}"
  ((count++))
  read -r -a editcmd <<< "${orbspec[$count]}"
  ((count++))
  IFS="$OIFS"
  
  for command in "${editcmd[@]}"
  do
    if [[ "${orbspec[$count]}" != "" ]]
      ## We dont set the entered data unless something was actually entered.
      then
      ## Sets the custom command to be run.
      buildopts["$command"]="${orbspec[$count]}"
    fi
    ((count++))
    # if [[ "${orbspec[$count]}" != "" ]]
      ## We dont set the entered data unless something was actually entered.
      # then
      ## Sets the prefix of the command to be run.
      buildopts["$command"-pre]="${orbspec[$count]}"
    # fi
    ((count++))
    # if [[ "${orbspec[$count]}" != "" ]]
      ## We dont set the entered data unless something was actually entered.
      # then
      ## Sets the suffix of the command to be run.
      buildopts["$command"-suf]="${orbspec[$count]}"
    # fi
    ((count++))
  done
}
libbuildfunctions+=("orbspecbuild")
libbuildvariables+=("OIFS")

write_pkginfo()
## This writes ${pkg[name]}_info function into the orb file.
## It stores global information for the package to be used in all builds.
## If a build needs to use alternative settings the build settings can
## override the global setting.
## This would be used when creating a build for a new version.
## The global version would be the default version of course.
{
  ## Writes the bash header to the orb file.
  { ## Using these brackets allows me to write all outputs in order more clearly
    printf '%s\n\n' "#!/usr/bin/env bash"
    printf '%s_%s%s\n%s\n' "${pkg[name]}" "info" "()" "{"
    printf '  %s\n' "declare -Ag pkg=("
    printf '    %s%s\n' "[name]=" "\"${pkg[name]}\""
    printf '    %s%s\n' "[version]=" "\"${pkg[version]}\""
    printf '    %s%s\n' "[extension]=" "\"${pkg[extension]}\""
    printf '    %s%s\n' "[description]=" "\"${pkg[description]}\""
    printf '    %s%s\n' "[homepage]=" "\"${pkg[homepage]}\""
    printf '    %s%s\n' "[dlurl]=" "\"${pkg[dlurl]}\""
    printf '    %s%s\n' "[url]=" "\"${pkg[url]}\""
    printf '    %s%s\n' "[file]=" "\"${pkg[file]}\""
    printf '    %s%s\n' "[bdeps]=" "\"${pkg[bdeps]}\""
    printf '    %s%s\n' "[cdeps]=" "\"${pkg[cdeps]}\""
    printf '    %s%s\n' "[deps]=" "\"${pkg[deps]}\""
    printf '    %s%s\n' "[odeps]=" "\"${pkg[odeps]}\""
    printf '    %s%s\n' "[checksum]=" "\"${pkg[checksum]}\""
    printf '  %s\n' ")"
    printf '%s\n\n' "}"
    printf '%s%s_%s%s\n\n' "info+=(\"" "${pkg[name]}" "info" "\")"
  } > "$orb"
}
libbuildfunctions+=("write_pkginfo")

write_build()
## Write the information to both the cmd array for the order of the commands
## as well as the buildopts associative array.
{
  ## Write the build function name.
  if [[ "$buildsuffix" != "" ]]
    then
    printf '%s_%s%s\n%s\n' "${pkg[name]}" "$buildsuffix" "()" "{" >> "$orb"
    buildfunc="${pkg[name]}_$buildsuffix"
  else
    printf '%s%s\n%s\n' "${pkg[name]}" "()" "{" >> "$orb"
    buildfunc="${pkg[name]}"
  fi
  
  ## Start writing the commands for the build.
  printf '%s\n\n' "  bdesc=\"$bdesc\"" >> "$orb"
  
  for key in "${pkgmod[@]}"
  do
    printf '%s\n' "  pkg[\"$key\"]=\"${pkg[$key]}\"" >> "$orb"
    r="yes"
  done
  
  if [[ "$r" = "yes" ]]
    ## Check if we need a return character. This only happens if something was
    ## written in the previous for loop.
    then
    printf '\n' >> "$orb"
    unset -v r
  fi
  
  printf '%s\n' "  buildcmd=(" >> "$orb"
  for command in "${buildcmd[@]}"
  do
    printf '%s\n' "    \"$command\"" >> "$orb"
  done
  
  {
    printf '%s\n\n' "  )"
    
    ## Write the buildopts associative array.
    printf '%s\n' "  declare -Ag buildopts=("
  } >> "$orb"
  
  ## Write the build section.
  for command in "${buildcmd[@]}"
  do
    { ## Using these brackets allows me to write all outputs in order more clearly
      printf '%s\n' "    [$command-pre]=\"${buildopts[$command-pre]}\""
      printf '%s\n' "    [$command]=\"${buildopts[$command]}\""
      printf '%s\n' "    [$command-suf]=\"${buildopts[$command-suf]}\""
    } >> "$orb"
  done
  
  {
    printf '%s\n' "  )"
    printf '%s\n\n' "}"
    
    ## Add the entry to the builds array.
    printf '%s%s%s\n\n' "builds+=(\"" "$buildfunc" "\")"
  } >> "$orb"
}
libbuildfunctions+=("write_build")
libbuildvariables+=("buildfunc" "key" "command")

getpkginfo()
## A meta task for orbspecpkginfo, ask_pkginfo, write_pkginfo
{
  if [[ "$1" != "" ]]
    ## If a parameter has been sent load the spec file.
    ## Otherwise use the interactive method.
    then
    orbspecpkginfo
  else
    ask_pkginfo
  fi
  write_pkginfo ## Write the package info.
}
libbuildfunctions+=("getpkginfo")

getbuildinfo()
## A meta task for orbspecbuild, ask_build, write_build
## Works the same way as getpkginfo, They are seperate because getbuildinfo
## needs to be looped.
{
  if [[ "$1" != "" ]]
    then
    orbspecbuild
  else
    ask_build
  fi
  write_build
}
libbuildfunctions+=("getbuildinfo")

loadorb() ## Accepts the name of a package and loads that orb file.
{
  orb="$orbs/$1/orb"
  if [[ -f "$orb" ]]
    then
    # shellcheck source=/dev/null
    . "$orb"
  fi
}
libbuildfunctions+=("loadorb")

lpi()
{
  "$1_info"
}
libbuildfunctions+=("lpi")

addbuild()
## Takes one argument of the name of an existing build file.
## Add a build to an existing orb file.
{
  loadorb "$1"
  lpi "$1"
  
  #export fakeroot="/fakeroot/${pkg[name]}-${pkg[version]}"
  # export fakeroot
  orb="$orbs/${pkg[name]}/orb" ## Set the location of the orb file.
  spec="$orb.spec" ## Set the location of the orb.spec file.
  
  backup_files "$spec"
  
  head -n -1 "$lastbackup" > "$spec"

  printf '%s\n' "y" >> "$spec"
  
  for (( ; ; ))
  do
    lpi "${pkg[name]}"
    getbuildinfo "$spec_file" ## Get build info. See getpkginfo comment.
    rqmsg "Would you like to make another build(Default=No)?"
    ## Ask if the user wants to create another build type.
    read -r answer
    msg " "
    printf '%s\n' "$answer" >> "$spec"
    if [[ "$answer" != "yes" ]] && [[ "$answer" != "Yes" ]] \
       && [[ "$answer" != "Y" ]] && [[ "$answer" != "y" ]]
      ## Only create another if the user asks swers some sort of yes.
      then
      break ## The user didnt want to do another iteration so break out.
    fi
  done
}
libbuildfunctions+=("addbuild")

neworb()
## Accepts no parameters for the interactive method.
## Accepts a package name with an existing orb.spec file in its orb directory.
## If no existing orb.spec exists it falls back to using the interactive method.
## A meta task for pkginfo and buildinfo.
## It accepts the creation of multiple builds.
{
  unset -v spec_file
  if [[ "$1" != "" ]]
    ## Check if the first parameter is not blank.
    then
    spec_file="$orbs/$1/orb.spec" ## Set the spec file location.
    if [[ -f "$spec_file" ]]
      ## Check if the spec file exists.
      then
      readorbspec "$spec_file" ## Read the orb.spec file into the orbspec array.
    else
      ## If the spec file is not a valid one. Act as if no parameters were sent.
      spec_file=""
    fi
  fi
  getpkginfo "$spec_file" ## Get the pkg info interactively or by orb.spec file.
  loadorb "${pkg[name]}"
  for (( ; ; ))
  do
    lpi "${pkg[name]}"
    #export fakeroot="/fakeroot/${pkg[name]}-${pkg[version]}"
    # export fakeroot
    getbuildinfo "$spec_file" ## Get build info. See getpkginfo comment.
    if [[ "$spec_file" = "" ]]
      ## Only ask the question if we are not pulling the information from a
      ## orb.spec file.
      then
      msg " "
      qmsg "Would you like to make another build(Default=No)?"
      ## Ask if the user wants to create another build type.
      read -r answer
      msg " "
      printf '%s\n' "$answer" >> "$spec"
    else
      ## If we read the info from a orb.spec file then set the answer and
      ## increment the count.
      answer="${orbspec[$count]}"
      ((count++))
    fi
    if [[ "$answer" != "yes" ]] && [[ "$answer" != "Yes" ]] \
       && [[ "$answer" != "Y" ]] && [[ "$answer" != "y" ]]
      ## Only create another if the user asks swers some sort of yes.
      then
      break ## The user didnt want to do another iteration so break out.
    fi
  done
}
libbuildfunctions+=("neworb")
libbuildvariables+=("answer" "spec_file")

buildsrcorb()
## Accepts two arguments. 
## The first argument must be the name of the package.
## The second must be the name of the build.
{
  unset pdcount
  pd "$src"
  loadorb "$1" ## Load the orb file.
  lpi "$1" ## Loads the package information.
  ## I dont need getsource yet but will later.
  # getsource ## Download the sources before trying to build.
  loadmod "$2" ## Loads any optional module files.
  "$1_$2" ## Loads the build to run.
  #export fakeroot="/fakeroot/${pkg[name]}-${pkg[version]}"
  #export fakeroot
  mkdir -p "$fakeroot"
  rqmsgr "Information of build:"
  ppi ## Print the package information.
  qmsgr "Build Description:"
  qmsgr "$bdesc"
  
  if [ ! -f "$src/${pkg[file]}" ]
  then
    while [ ! -f "$src/${pkg[file]}" ]
    do
      sleep 1s
    done
    sleep 1s
  fi
  while [ -f "$src/.${pkg[file]}.lock" ]
  do
    sleep 1s
  done
  
  buildlog="build-$(date +%Y%m%d-%H%M%S).log"
  msg "Running build for $1"
  mkdir -p "$log/$1"
  msg "Build log location: $log/$1/$buildlog"
  for command in "${buildcmd[@]}"
  do
    ## put the prefix and suffix information into an array.
    read -r -a pre <<< "${buildopts[$command-pre]}"
    read -r -a suf <<< "${buildopts[$command-suf]}"
    
    ## Run the actual command.
    eval "${pre[@]}" "${buildopts[$command]}" "${suf[@]}" >> "$log/$1/$buildlog"
  done
  pds
}
libbuildvariables+=("pre" "suf" "buildlog" "fakeroot")
libbuildfunctions+=("buildsrcorb")

buildgroup()
## Accepts one argument. The argument must be a valid group file.
{
  # shellcheck disable=SC2154
  groupfile="$groups/$1"
  if [[ -f "$groupfile" ]]
    then
    while IFS= read -r -a line
    do
      read -r -a args <<< "$line"
      buildsrcorb "${args[@]}"
    done < "$groupfile"
  fi
}
libbuildfunctions+=("buildgroup")

aoi()
## Will print out the information in all of the loaded orb files.
{
  for infofunc in "${info[@]}"
  ## Cycles through the different orb file info sections loaded.
  do
    local infomsg=("Loading" "$infofunc" "...")
    ## Print what we're loading.
    qmsg "${infomsg[@]}"
    msg " " " "
    "$infofunc" ## Load the pkg array from the current info loaded.
    ppi
  done
}
libbuildfunctions+=("aoi")
libbuildvariables+=("infomsg")

pkgbin()
{
  loadorb "$1"
  lpi "$1"
  "$1_$2"
  pd /fakeroot/"${pkg[name]}-${pkg[version]}"
  mkdir -p "$archives"
  if [[ "$2" != "" ]]
  then
    pkgname="${pkg[name]}-$2-${pkg[version]}"
  else
    pkgname="${pkg[name]}-${pkg[version]}"
  fi
  tar -cJpf "$archives"/"$pkgname".tar.xz ./
  pds
}
libbuildfunctions+=("pkgbin")
libbuildvariables+=("pkgname")

# epkg()
# {
#   return
# }
# libbuildfunctions+=("epkg")
# 
# ipkg()
# {
#   return
# }
# libbuildfunctions+=("ipkg")

orbbin()
{
  loadorb "$1"
  lpi "$1"
  "$1_$2"
  mkdir -p "$modules"
  if [[ "$2" != "" ]]
  then
    pkgname="${pkg[name]}-$2-${pkg[version]}"
  else
    pkgname="${pkg[name]}-${pkg[version]}"
  fi
  mksquashfs "$fakeroot/${pkg[name]}-${pkg[version]}/" "$modules/$pkgname.orb" -comp lz4
}
libbuildfunctions+=("orbbin")

# eorb()
# {
#   return
# }
# libbuildfunctions+=("eorb")
# 
# iorb()
# {
#   return
# }
# libbuildfunctions+=("iorb")

oi() ## Takes one argument of a pkg name.
{
  infofunc="$1_info"

  local infomsg=("Loading " "$infofunc" "...")
  ## Print what we're loading.
  qmsg "${infomsg[@]}"
  msg " " " "
  "$infofunc" ## Load the pkg array from the current info loaded.
  ppi
}
libbuildfunctions+=("oi")

cleanup_libbuild()
## Clean up everything to do with lib-build.
{
  cleanup_orbfiles ## The orbfiles are only used in this code so we unset it.
  
  ## Unset the functions.
  for function in "${libbuildfunctions[@]}"
  do
    unset -f "$function"
  done
  ## Unset the variables. "_" contains us running cleanup_libbuild
  ## We clean up so the terminal is exactly the same as if you had
  ## opened a new one.
  libbuildvariables+=("_" "var")
  for var in "${libbuildvariables[@]}"
  do
    unset -v "$var"
  done

  cleanup_orbosconf ## We pull in orbosconf at the top of the file so we unset it.
  unset -f "cleanup_libbuild"
}
